const { multiply, add } = require('../lib/matrix')

describe('multiply', () => {
  test('it should return a funtion that multiplies a 1x2 by a 2x1 matrix', () => {
    expect(multiply(1,2,2,1)([[1], [20]], [[300, 4000]]))
      .toEqual([[300,4000],[6000, 80000]])
  })

  test('it should return a funtion that multiplies a 3x1 to a 3x3 matrix', () => {
    expect(multiply(3,1,3,3)([[2,3,5]],[[7,11,13],[17,19,23],[29,31,37]]))
      .toEqual([[210,234,280]])
    expect(multiply(3,1,3,3)([[41,43,47]],[[53,59,61],[67,71,73],[79,83,89]]))
      .toEqual([[8767,9373,9823]])
    expect(multiply(3,1,3,3)([[1,2,62]],[[7,-56,13],[6,8,23],[-29,31,-37]]))
      .toEqual([[-1779,1882,-2235]])
  })

  test('it should return a funtion that multiplies a 3x1 to a 3x3 matrix', () => {
    expect(multiply(3,3,3,3)([[11,22,33],[44,55,66],[77,88,99]],[[5,7,11],[13,17,19],[23,29,31]]))
      .toEqual([[1100,1408,1562],[2453,3157,3575],[3806,4906,5588]])
  })
})


describe('add', () => {
  test('it should return a funtion that adds two 3x1 matricies', () => {
    expect(add(3,1)([[1,2,3]],[[10,100,1000]]))
      .toEqual([[11,102,1003]])
  })
})


